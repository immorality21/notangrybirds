﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class FiringManager : MonoBehaviour
{
    [SerializeField]
    private Slider _forceSlider;
    [SerializeField]
    private Slider _directionSlider;

    [SerializeField]
    private float _forceChangeSpeed = 750;

    [Space]

    [SerializeField]
    private GameObject _projectilePrefab;
    [SerializeField]
    private GameObject _childPrefab;
    [SerializeField]
    private float _projectileLifeSpan = 3.5f;
    [SerializeField]
    private Gradient _projectileColorGradient;
    [SerializeField]
    private Vector3 _spawnPosition = new Vector3(0, 1.6f, -8f);

    private GameObject _currentProjectile { get; set; }
    private bool _canFire { get; set; }

    private bool _mouseButtonDown { get; set; }
    private Vector3 _lastMousePos { get; set; }

    private void Start()
    {
        PlaceNew();
    }

    private void Update()
    {
        if (_canFire && Input.GetMouseButtonDown(0))
        {
            _mouseButtonDown = true;
            _lastMousePos = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0))
        {
            Fire();
        }

        if (_currentProjectile != null && Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(SpawnMiniProjectiles());
        }

        UpdateDirectionSlider();

        UpdateForceSlider();
        
        UpdateFireAngle();

        MoveProjectile();
    }

    private void UpdateForceSlider()
    {
        _forceSlider.value = Mathf.PingPong(Time.time * _forceChangeSpeed, _forceSlider.maxValue);
    }

    private void UpdateDirectionSlider()
    {
        if (!_canFire)
        {
            return;
        }

        if (Input.GetKey(KeyCode.A))
        {
            _directionSlider.value -= 0.1f;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            _directionSlider.value += 0.1f;
        }
    }

    private void MoveProjectile()
    {
        if (_currentProjectile == null)
        {
            return;
        }

        var newPosition = new Vector3(_directionSlider.value, _currentProjectile.transform.position.y, _currentProjectile.transform.position.z);

        _currentProjectile.transform.position = newPosition;
    }

    private void UpdateFireAngle()
    {
        if (!_mouseButtonDown)
        {
            return;
        }

        var mousePos = Input.mousePosition;

        // Change in vertical angle
        if (!Mathf.Approximately(_lastMousePos.y, mousePos.y))
        {
            float yAngle = _lastMousePos.y > mousePos.y
                ? 1f
                : -1f;

            _currentProjectile.transform.Rotate(Vector3.right, yAngle, Space.World);
        }

        _lastMousePos = Input.mousePosition;
    }

    private void PlaceNew() 
    {
        if (_currentProjectile != null)
        {
            return;
        }

        _currentProjectile = Instantiate(_projectilePrefab, _spawnPosition, Quaternion.identity);
        var rigidBody = _currentProjectile.GetComponent<Rigidbody>();
        rigidBody.isKinematic = true;

        // Randomize mass and color
        float mass = Random.Range(1f, 7f);
        rigidBody.mass = mass;

        float time = (mass - 1) / 7;

        _currentProjectile.GetComponent<MeshRenderer>().material.color = _projectileColorGradient.Evaluate(time);

        // Randomize size
        _currentProjectile.transform.localScale = new Vector3(1, 1, Random.Range(1f, 4f));

        _canFire = true;
        _directionSlider.value = 0;
    }

    public void Fire()
    {
        if (!_canFire)
        {
            return;
        }

        _canFire = false;
        _mouseButtonDown = false;

        var rigidBody = _currentProjectile.GetComponent<Rigidbody>();
        rigidBody.isKinematic = false;

        float forceSliderValue = _forceSlider.value < 10
            ? 10
            : _forceSlider.value;

        rigidBody.AddRelativeForce(0, 0, forceSliderValue, ForceMode.Impulse);

        StartCoroutine(DelayedDestroy(_currentProjectile));
    }

    private IEnumerator SpawnMiniProjectiles()
    {
        var position = _currentProjectile.transform.position;
        Destroy(_currentProjectile);

        for (int i = 0; i < 5; i++)
        {
            yield return new WaitForSeconds(0.05f);

            var child = Instantiate(_childPrefab, position, Quaternion.identity);

            float x = Random.Range(-25, 25);
            float y = Random.Range(-25, 25);
            float z = Random.Range(-25, 25);

            child.GetComponent<Rigidbody>().AddRelativeForce(x, y, z, ForceMode.Impulse);

            StartCoroutine(DelayedDestroy(child));
        }
    }

    private IEnumerator DelayedDestroy(GameObject toBeDestroyedObject)
    {
        yield return new WaitForSeconds(_projectileLifeSpan);

        if (_currentProjectile != null)
        {
            Destroy(toBeDestroyedObject);
        }

        PlaceNew();
    }
}
